# Desio: Statistiche Polizia Locale

https://statistiche-polizia-desio.netlify.com

Ogni grafico è dotato di un link che ne permette il referenziamento diretto.

Questo progetto si basa su [questo dataset](https://github.com/comune-desio/opendata/blob/feature/polizia-attivita/polizia-locale/attivita.csv) e usa i seguenti strumenti:

- [Brunch](https://www.brunch.io): per creare il sito statico
- [Chart.js](http://www.chartjs.org): per disegnare i grafici
- [Papaparse](http://papaparse.com/): per interpretare il dataset CSV

Developed with <3 by [@olistik](https://olisti.co).

## Installation

Clone this repo manually or use `brunch new dir -s brunch/with-es6`

## Getting started

* Install (if you don't have them):
    * [Node.js](http://nodejs.org): `brew install node` on OS X
    * [Brunch](http://brunch.io): `npm install -g brunch`
    * Brunch plugins and app dependencies: `npm install`
* Run:
    * `brunch watch --server` — watches the project with continuous rebuild. This will also launch HTTP server with [pushState](https://developer.mozilla.org/en-US/docs/Web/Guide/API/DOM/Manipulating_the_browser_history).
    * `brunch build --production` — builds minified project for production
* Learn:
    * `public/` dir is fully auto-generated and served by HTTP server.  Write your code in `app/` dir.
    * Place static files you want to be copied from `app/assets/` to `public/`.
    * [Brunch site](http://brunch.io), [Getting started guide](https://github.com/brunch/brunch-guide#readme)

## License

See the [LICENSE](/LICENSE) file for more info.
