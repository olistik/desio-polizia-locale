import 'whatwg-fetch';
import Chart from 'chart.js';

function navigateToCurrentChart() {
  let hash = `${window.location.hash}`
  if (hash !== "") {
    document.querySelector(`[name=${hash.slice(1)}]`).nextElementSibling.classList.add("selected")
    window.location = window.location.hash;


  }
}

function renderChart(container, chart) {
  let anchor = document.createElement("a")
  anchor.name = `chart-${chart.uuid}`
  anchor.href = `#${anchor.name}`
  anchor.text = chart.name
  container.appendChild(anchor)
  let ctx = document.createElement("canvas")
  ctx.width = 500
  ctx.height = 200
  container.appendChild(ctx)
  let myChart = new Chart(ctx, {
      type: "line",
      data: {
        labels: chart.dataFields,
        datasets: [{
          label: false,
          data: chart.data,
          fill: false,
          lineTension: 0,
          backgroundColor: "#aa2106",
          borderWidth: 2,
          borderColor: "#ea2106"
        }]
      },
      options: {
        responsive: false,
        legend: {
            display: false
        },
        tooltips: {
          callbacks: {
            label: function(tooltipItem) {
              return `Valore: ${tooltipItem.yLabel}`;
            }
          }
        },
      }
  });

}
function renderCharts(results) {
  let rows = results.data
  let fields = results.meta.fields
  let uuidField = fields[0]
  let headerField = fields[1]
  let dataFields = fields.slice(2).reverse()
  let charts = rows.map(row => {
    return {
      uuid: row[uuidField],
      name: row[headerField],
      data: dataFields.map(field => row[field]),
      dataFields: dataFields,
    }
  })
  let container = document.querySelector(".charts")
  charts.forEach(chart => renderChart(container, chart))
  navigateToCurrentChart()
}

function fetchDataset() {
  const DATASET_URL = "https://raw.githubusercontent.com/comune-desio/opendata/feature/polizia-attivita/polizia-locale/attivita.csv";
  fetch(DATASET_URL)
    .then(response => response.text())
    .then(text => text.trim())
    .then((text) => {
      let results = Papa.parse(text, {
        delimiter: ",",
        header: true,
        complete: renderCharts,
        error: () => {
          console.log("error while parsing the dataset");
        }
      });
    }).catch(() => { console.log("Errors while fetching the dataset.")});
}

document.addEventListener("DOMContentLoaded", () => {
  console.log("Init")
  fetchDataset()
});
